# Website for H2020 - Cloud Perfect

# Local development

Clone the repository and either run 
```
./hugo_0.18_linux_amd64 server
```
or
```
./hugo_0.18_windows_amd64.exe server
```
or
```
./hugo_0.18_darwin_amd64 server
```
in your terminal

# Deploy

Just commit your changes and push them to the master. CI takes care of building and deployment

# Edit content

See the [Wiki](https://omi-gitlab.e-technik.uni-ulm.de/CloudPerfect/website/wikis/home) for more information

