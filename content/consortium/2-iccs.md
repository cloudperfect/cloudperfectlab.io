+++
weight = 2
type = "consortium"
name = "Institute of Communications and Computer Systems"
abbr = "ICCS"
logo = "img/logos/iccs.png"
website = "http://www.iccs.gr/eng/"

description = """
The National Technical University of Athens (NTUA) is the oldest and most prestigious technical university in Greece. It was founded in 1837 and has since been contributing to the progress of the engineering science in Greece, through the education of young engineers and its multi-faceted research and development activities. The University comprises nine departments, each one covering a different aspect of the engineering field. The School of Electrical and Computer Engineering (ECE) of NTUA is well known in Greece and abroad for the research achievements of its faculty members and the good reputation of its students and alumni. The field of Electrical and Computer Engineering spans a wide range of subject areas, like computer science, telecommunications, electronics, automatic control and electric power. The Institute of Communication and Computer Systems-ICCS is a research organization associated with the ECE School and has about 40 laboratories and research units. ICCS/NTUA will participate in CLOUDPERFECT through the Distributed, Knowledge and Media Systems Group (DKMS) that focuses on research activities related to advanced distributed computing, dealing with topics such as Service Oriented Architectures, Cloud Computing, Performance Analysis and Modelling, Internet of Services and Things and Service Level Agreements.
"""

summary = """
ICCS will perform the project’s technical coordination, as well as lead the work on architecture through WP2 and standardization. ICCS has a vast experience in managing related projects (technical coordinator of FP7 IRMOS and COSMOS) as well as active participation in standardization fora, actively contributing at the moment in ISO/IEC JTC 1/SC 38/WG 3 and the ongoing 19086 draft standard series on Cloud Service Level Agreements. ICCS will also provide the prototypes of 3ALib, Benchmarking Suite GUI and Profilling/Classification tool from ARTIST project, as well as the prediction model creation algorithm from the IRMOS project, for participation in the technical activities of the project, as well as the pilot execution. ICCS has actively participated in the provider benchmarking and usage of that data for application management and provider selection processes. ICCS has also extensive experience in Service Level Agreements and metrics specification, through its participation in the H2020 SLALOM project and in the activities of the SPEC Cloud WG. ICCS has also experience in Cloud optimization strategies and VM allocation through its participation in FP7 OPTIMIS.
"""

role = """
ICCS will perform the project’s technical coordination, as well as lead the work on architecture through WP2 and standardization. ICCS has a vast experience in managing related projects (technical coordinator of FP7 IRMOS and COSMOS) as well as active participation in standardization fora, actively contributing at the moment in ISO/IEC JTC 1/SC 38/WG 3 and the ongoing 19086 draft standard series on Cloud Service Level Agreements. ICCS will also provide the prototypes of 3ALib, Benchmarking Suite GUI and Profilling/Classification tool from ARTIST project, as well as the prediction model creation algorithm from the IRMOS project, for participation in the technical activities of the project, as well as the pilot execution. ICCS has actively participated in the provider benchmarking and usage of that data for application management and provider selection processes. ICCS has also extensive experience in Service Level Agreements and metrics specification, through its participation in the H2020 SLALOM project and in the activities of the SPEC Cloud WG. ICCS has also experience in Cloud optimization strategies and VM allocation through its participation in FP7 OPTIMIS.
"""

publications = [
"publication1",
"publication2"
]

[[projects]]
name = "SLALOM"
description = """
Leader of Technical Track in H2020 SLALOM (http://slalom-project.eu/). SLALOM targets at creating an Open Specification model for Cloud SLAs including standardized terms and metrics descriptions
"""
[[projects]]
name = "ARTIST"
description = """
Participation in FP7 project ARTIST (http://www.artist-project.eu/). ARTIST aimed to offer a set of methods and tools which provide an end-to-end and assisted migration service to transform non-cloud software applications not merely to run on cloud but to take full advantage of cloud features
"""
[[projects]]
name = "IRMOS"
description = """
Technical Management in FP7 project IRMOS (http://www.irmosproject.eu), dealing with real time multimedia applications running on top of virtualized service oriented infrastructures
"""
[[projects]]
name = "OPTIMIS"
description = """
Participation in FP7 project OPTIMIS (http://www.optimis-project.eu/) with relation to optimal grouping of VMs to physical nodes based on Trust, Risk, Eco and Cost parameters
"""

[[personnel]]
name = "Prof. Theodora A. Varvarigou"
gender = "female"
description = """
eceived her B. Tech. degree from NTUA in 1988, her MSc degree in electrical engineering in 1989 and PhD degree in computer science in 1991, both from Stanford University. She worked at AT&T Bell Labs in New Jersey between 1991 and 1995. Between 1995 and 1997 she worked as an Assistant Professor at the Technical University of Crete. In 1997, she joined NTUA as an assistant professor, and became a full professor in 2007. She has extensive experience in the area of semantic web technologies, scheduling over distributed platforms, embedded systems and grid computing, and has published more than 150 papers in leading journals and conferences. She has participated in and coordinated several EU funded projects including 4CaaSt, OPTIMIS, VISION Cloud, IRMOS, POLYMNIA, Akogrimo, NextGRID, BEinGRID, EchoGRID, SCOVIS, MEMPHIS, and CHALLENGERS.  
"""

[[personnel]]
name = "Dr. George Kousiouris"
gender = "male"
description = """
received his Msc in Electrical and Computer Engineering from the University of Patras, Greece in 2005 and his Ph.D. in Cloud Computing at the Telecommunications Laboratory of the Dept. of Electrical and Computer Engineering of the National Technical University of Athens in 2012. He is a researcher for the Institute of Communication and Computer Systems (ICCS), where he has participated in the EU funded projects COSMOS (as lead architect), ARTIST (as the main 3ALib developer), OPTIMIS, IRMOS and BEinGRID and national projects. His interests are mainly Service Level Agreements, computational intelligence, performance and description modelling, distributed systems and service oriented architectures.
"""

[[personnel]]
name = "Dr. Fotis Aisopos"
gender = "male"
description = """
"""

[[personnel]]
name = "Mr. Alexandros Psychas"
gender = "male"
description = """
"""
#[[personnel]]
#name = "Dr. Kleopatra Konstanteli"
#gender = "female"
#description="""
#received her diploma in Electrical and Computer Engineering in 2004 by the National Technical University of Athens (NTUA). In 2007 she received a Master in Techno-economical Systems (NTUA, University of Athens, and University of Piraeus), and her Ph.D. in the area of distributed computing from the school of Electrical and Computer Engineers of NTUA in 2011. She has been working in the Telecommunications Laboratory of Electrical and Computer Engineering of NTUA and participating in EU funded projects since 2005. Her research interests are mainly focused in the field of distributed computing and optimization. She participated in the EU funded projects ARTIST, EXPERTMEDIA, OPTIMIS, IRMOS and Akogrimo and other national projects.
#"""
#
#[[personnel]]
#name = "Athanasia Evangelinou"
#gender = "female"
#description = """
#received her diploma in Electrical and Computer Engineering from the National Technical University of Athens (NTUA), Greece in 2012. She received her Ms.C. in Network-Oriented Information Systems from the Dept. of Digital Systems of the University of Piraeus, Greece in 2014. Currently she is a Ph.D. candidate at the Distributed Knowledge and Media Systems group of the School of Electrical and Computer Engineering (NTUA). Her research interests are in the area of Cloud Computing with emphasis on Cloud Performance analysis, Benchmarking and Optimization.
#"""
+++

