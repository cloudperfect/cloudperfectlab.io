+++
weight = 6
type = "consortium"
name = "COSMOTE Mobile Telecommunications S.A"
abbr = "COS"
logo = "img/logos/cos.png"
website = "https://www.cosmote.gr/"

description = """
COSMOTE (http://www.cosmote.gr) is a member of OTE Group and a member of Deutsche Telekom Group (since March/09). It was commercially launched in April/98 and within a short period of time COSMOTE succeeded in becoming the leading provider of mobile telecommunications services in Greece. Today COSMOTE accounts for more than 50% of the Greek subscribers’ base -exceeding 7.4 million (Q4/2016) - which together with its subsidiaries in the in SE Europe (Romania and Albania), forms one of the leading telecommunications groups, with more than 22,000 employees. COSMOTE holds the leading market position in Greece since 2001, having an impressive record of very important firsts in Greek and/or European Market: (a) HSDPA (June/06), (b) HSPA+ (May/09), (c) MBB at 28.8 Mbps (Mar/10), (d) MBB at 42.2 Mbps (adopting HSPA+ Dual Carrier) in real network conditions (May/10), (e) demonstrated speeds of 100 Mbps (DL)/45Mbps (UL) using commercial LTE terminals, (f) 4G/LTE services (Nov/12), (g) 4G+/LTE-A services with speeds above 300Mbps (Feb/15) and 500Mbps (Dec/16).

The OTE Group is a pioneer in technological advances and by far the largest investor in telecommunications infrastructure in Greece. During 2016, the OTE Group participated in 23 innovative funded R&D projects (incl. 4 5G-PPP projects), which are in line with its sustainability values and with a high level of community impact. By participating in R&D projects, the OTE Group is expected to boost its technological excellence –one of the OTE Group’s strategic pillars-, impact on future products/services, technologies and telecom network, and facilitate the identification of new business opportunities to strengthen Group’s competitiveness.
"""

summary = """
COSMOTE (http://www.cosmote.gr) is a member of OTE Group and a member of Deutsche Telekom Group (since March/09). It was commercially launched in April/98 and within a short period of time COSMOTE succeeded in becoming the leading provider of mobile telecommunications services in Greece. Today COSMOTE accounts for more than 50% of the Greek subscribers’ base -exceeding 7.4 million (Q4/2016) - which together with its subsidiaries in the in SE Europe (Romania and Albania), forms one of the leading telecommunications groups, with more than 22,000 employees. COSMOTE holds the leading market position in Greece since 2001, having an impressive record of very important firsts in Greek and/or European Market: (a) HSDPA (June/06), (b) HSPA+ (May/09), (c) MBB at 28.8 Mbps (Mar/10), (d) MBB at 42.2 Mbps (adopting HSPA+ Dual Carrier) in real network conditions (May/10), (e) demonstrated speeds of 100 Mbps (DL)/45Mbps (UL) using commercial LTE terminals, (f) 4G/LTE services (Nov/12), (g) 4G+/LTE-A services with speeds above 300Mbps (Feb/15) and 500Mbps (Dec/16).

The OTE Group is a pioneer in technological advances and by far the largest investor in telecommunications infrastructure in Greece. During 2016, the OTE Group participated in 23 innovative funded R&D projects (incl. 4 5G-PPP projects), which are in line with its sustainability values and with a high level of community impact. By participating in R&D projects, the OTE Group is expected to boost its technological excellence –one of the OTE Group’s strategic pillars-, impact on future products/services, technologies and telecom network, and facilitate the identification of new business opportunities to strengthen Group’s competitiveness.
"""

role = """
COSMOTE participates with a dedicated high-skilled R&D team with experience in EU and national funded technology-oriented projects in various domains, such as: technology interoperability, SDN/NFV, cloud computing, QoS/QoE, network optimization, terminal/network security, next generation emergency services, ITS.  In the context of the project, COSMOTE will focus on the description of use cases, the definition of requirements and the use cases business and technical SLAs. Moreover, COSMOTE will assist at defining the overall system architecture and participate in the design phases of the solution, including services abstraction, automation and virtualization. COSMOTE will also provide to the project the Cloud testing infrastructure located in Athens and contribute to the design and integration of the cloud platform and applications and the validation of the overall system as well as the Public Cloud division experience during the requirements elicitation process and pre-market evaluation. Finally, COSMOTE will participate in the dissemination and the communication activities of the project and provide significant input to the identification of the feasible exploitation plan.
"""

contributions = """
COSMOTE will participate in the experimental phase as a Cloud provider through its testing facility. The testbed is setup and configured with Ubuntu Server 14.04 LTS, as one of the most active providing Long Term Support and ensuring that any issues that come up within the next five years will be dealt with, and Openstack. The latter is one of the most active projects, providing close to commercial features, expendability and good documentation, CMD and GUI control and moreover integrates well with Openflow, allowing the creation for complex architectures including both cloud and SDN technologies. This testbed is also interconnected with COSMOTE’s other labs, providing many capabilities for testing new technologies either for PoC (Proof of Concept) or for systems aimed at the field. The testbed is developed and used in previous EU research projects, such as the SECCRIT, the SESAME, and the CREDENTIAL projects.
This testbed is also interconnected with COSMOTE’s other labs, providing many capabilities for testing new technologies either for PoC (Proof of Concept) or for systems aimed at the field. The testbed is developed and used in previous EU research projects, such as the SECCRIT, the SESAME, and the CREDENTIAL projects.


| **System** | **#CPUs** | **HD** | **RAM (GB)** | **eth onboard** | **eth installed** |
|---|---|---|---|---|---|
| Gateway | Dell T310 PE | 4 | 500GB 2,5" + 2TB 3,5'' | 8 | 2 | 1 |
| Controller 1 | Dell T310 PE | 4 | 1x 2TB, 1x 500 GB | 8 | 1 | 1 |
| Controller 2 | Dell Optiplex 9020 | 4 | 2TB | 8 | 2 | 1 |
| Compute 1 | Dell Optiplex 9020 | 4 | 2TB | 8 | 1 | 1 |
| Compute 2 | Dell Optiplex 9020 | 4 | 2TB | 8 | 1 | 1 |
| Compute 3 | Dell T320 PE | 4 | 1TB | 16 | 2 | 0 |

"""

[[projects]]
name = "VIMSEN"
description = """
(Virtual Microgrids for Smart Energy Networks, http://www.ict-vimsen.eu/): Aims at providing flexibility to small/very small energy generators, to re-distribute energy resources with each other to compensate energy production-distribution, and to directly participate in the electricity market through an energy aggregating association (which acts similarly as a big power generator unit) by utilising IoT technologies. In the scope of this project cloud architectures are investigated. COSMOTE contributes in the use cases and the communication system requirements identifications, the specifications definition, the trials design and the evaluation of results, as well as in the development of suitable business models. In the context of the project COSMOTE has developed a local Gateway with Communication and Energy Management IoT functionalities.
"""
[[projects]]
name = "INPUT"
description = """
(In Network Programmability for next-generation personal cloUd service support, http://input-project.eu/): Aims at designing a novel infrastructure and paradigm to support Future Internet personal cloud services in a more scalable and sustainable way and with innovative added-value capabilities. The INPUT technologies will enable next-generation cloud applications to be supported over distributed cloud architectures and go beyond classical service models. COSMOTE contributes to the use cases and requirements identification, the architecture and communication system specifications design as well as to cloud applications implementation.
"""
[[projects]]
name = "FLEX"
description = """
(FIRE LTE testbeds for open Experimentation, http://www.flex-project.eu/): FLEX aims at contributing cellular access technologies and LTE in particular in FIRE’s research platform for investigation and experimental evaluation of innovative ideas in networking and services. More specifically, programmable LTE components, management and experiment control tools will be built as extensions to existing European testbeds, specialized monitoring tools with focus on mobility and methodologies will also be developed, while two open calls will be organized, to attract research groups wishing to conduct sophisticated experiments, test innovative usages or provide functional extensions of the LTE testbeds. COSMOTE participates in the use case and requirements analysis and the physical deployment phase, has major contribution to design and development of LTE-specific monitoring tools and applications, the testing and evaluation phases while participating in the dissemination activities and the exploitation of the project results.
"""
[[projects]]
name = "i-PROGNOSIS"
description = """
i-PROGNOSIS (Intelligent Parkinson eaRly detectiOn Guiding NOvel Supportive InterventionS, http://www.i-prognosis.eu/): The cardinal objective of i-PROGNOSIS is the development of (i) an ICT-based behavioural analysis approach for capturing, as early as possible, the Parkinson’s Disease (PD) symptoms appearance, and (ii) the application of ICT-based interventions countering identified risks. To achieve this, awareness initiatives will be employed, so as to construct i-PROGNOSIS community, targeting > 5000 older individuals within the duration of the project, in order to unobtrusively sense large scale behavioural data from its members, acquired from their natural use of mobile devices (smartphone/ smartwatch). Ensuring anonymisation and secure Cloud archiving, i-PROGNOSIS will develop and employ advanced big data analytics and machine learning techniques, in a distributed and privacy aware fashion. To those identified and clinically validated as early stage PD patients, ICT-based interventions will be provided via the i-PROGNOSIS Intervention Platform, including: a) a Personalised Game Suite (ExerGames, DietaryGames, EmoGames, Handwriting/VoiceGames) for physical/emotional support, b) targeted nocturnal intervention to increase relaxation/sleep quality and c) assistive interventions for voice enhancement and gait rhythm guidance. COSMOTE contributes to the project’s use cases definition, requirements, specifications and overall system architecture focusing on the communication framework. In addition, it contributes to the validation and evaluation of the overall system as well as the successful dissemination and exploitation of the i-PROGNOSIS approach.
"""


[[personnel]]
name = "Dr. George Lyberopoulos"
gender = "male"
description = """
received the Electrical Engineers Diploma from the Democritus University of Thrace (DUTH), in 1989 and the Dr.-Ing. degree in Electrical Engineering from the National Technical University of Athens (NTUA), in 1994. Since 1989, he has been involved in several EU and national research projects (RACE R1043 MOBILE, RACE R2066 MONET, ACTS EXODUS, ACTS STORMS, ACTS ASPeCT, IST NETGATE, FP7-PANLAB II, FP7-ECOGEM, FP7-OPENLAB, FP7-GERYON, FP7-NEMESYS, FP7-TEAM, FP7-EMERALD, FP7-FLEX, FP7-VIMSEN, H2020-INPUT, (5G-PPP) 5G-XHaul, (5G-PPP) CHARISMA, i-PROGNOSIS, i-HeERO, ESPA-CONFES, ESPA-eCALL, ESPA-WIKIZEN). Dr. Lyberopoulos joined COSMOTE in 1999, while today he is heading the Research & Development Dept., Fixed and Mobile. Since 1999, he has been appointed Project/Technical manager for all the major projects within COSMOTE such as, GPRS, 3G, WiMAX, IMS, Femtocells, LTE including, tenders (specifications/evaluation), technical trials and network design & rollout phases. Dr. Lyberopoulos is author of over 50 scientific papers in the areas of mobile telecommunications.
"""

[[personnel]]
name = "M.Sc. Eleni Theodoropoulou"
gender = "female"
description = """
 is a graduate of the Physics Dept. of the National University of Athens (ΝUoA). In 1994 she received her M.Sc. in Radioelectrology and Electronics from the same University. She has worked with two Greek Mobile Operators (1994–beg.1998 Telestet Hellas and beg.1998–today COSMOTE). During 1994-2000 she was involved mainly in radio network planning. Heading the New Technologies & Special Projects Section (2000-2005), her main responsibilities included monitoring the technology evolution as well as the co-ordination of technology trials, and also special projects such as the management of the network planning team for the Olympic Sponsorship, Athens 2004. During 2005-2009, she has been responsible for the New Access Network Technologies Section with field of responsibility covering the areas of wireless broadband access and broadcasting technologies. Since Sept. 2009, she is heading the R&D Projects Mobile Section of the Research & Development Dept., Fixed & Mobile.
"""

[[personnel]]
name = "Dr. Konstantinos Filis"
gender = "male"
description = """
holds a Ph.D. degree from Southern Methodist University, Dallas, Texas, an MSEE degree from Purdue University, W. Lafayette, Indiana and a BSEE degree from Ohio University, Athens, Ohio. He is an OTE employee detached to COSMOTE since its establishment in 1996 and he has contributed in several projects, including the initial GSM radio network planning and testing, and the introduction and the initial radio network planning of UMTS. Currently he serves as a Senior Research Engineer. From 1995-1996 he held the position of Assistant Researcher in NCSR (National Center for Scientific Research) DEMOKRITOS, where he performed research on the co-existence of WCDMA cellular systems with FSM (Fixed Service Microwave) systems on the same frequency bands. From 1993-1995 he worked as a Telecommunications Engineer in INTRACOM SA, where his main duties included the assembly, laboratory testing, installation and on-site performance evaluation of point-to-point microwave systems. Dr. Filis has authored several scientific papers in the area of mobile communications.
"""

[[personnel]]
name = "M.Sc. Ioanna Mesogiti"
gender = "female"
description = """
received her Diploma in Electrical and Computer Engineering from the National Technical University of Athens (NTUA) in 2002. She holds an M.B.A. degree from Athens University of Economics and Business (AUEB) (in collaboration with NTUA) and an MSc degree in Telecommunications (Networks) from the University of London (Queen Mary College), U.K. During 2001–2002 she worked as a research associate in NCSR Demokritos and from 2003 through 2005 she was employed as a software engineer in Siemens S.A. in the Fixed Networks Department, where her main responsibilities were software analysis and development, as well as the functional verification and support of digital telephony networks (ITU-T Q.931 based) and H.323-based VoIP networks (ITU-T Q.931 (DSS1 part), H.323, Η.225 & Η.245 based). In 2005 she joined COSMOTE initially in the New Technologies and Special Projects Section and later in the New Technologies Sub-Department specializing in access network technologies and participating in projects such as vendor selection contests, consultations, and internal projects regarding specification, design, integration and testing of novel access technologies in COSMOTE’s network, and since 2009 in the R&D Section as Senior Engineer. Her fields of expertise include wireless network technologies and telecommunications protocols design, testing, and implementation. She is a member of the Technical Chamber of Greece (ΤΕΕ).
"""

+++

