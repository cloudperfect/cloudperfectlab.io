+++
weight = 3
type = "consortium"
name = "ICON Technology & Process Consulting Limited"
abbr = "ICON"
logo = "img/logos/icon.png"
website = "http://www.iconCFD.com"

description = """
ICON (ICON Technology & Process Consulting Limited) operates in the high-tech field of Computational Fluid Dynamics (CFD) and provides blue-chip multi-sector engineering companies, their suppliers and consultants with the ability to predict fluid flow using 3D computer simulation.  Since 1992, the company has been delivering tailor-made solutions and advice to OEMs, SMEs and research organisations worldwide. An independent approach ensures that customers always benefit from state-of-the-art computational methods, tools and advice. These qualities have made ICON a leading provider of engineering software and services to industry. The evolution of the company continues with recent growth in the fields of automatic optimisation and open source simulation technology.  ICON has a proven track record of delivering new and improved simulation processes to industrial customers.  For example, ICON has been a lead partner to industry in delivering the innovative Open Source CFD simulation processes currently installed at over 50% of the top 20 automotive manufacturers in Europe, America and Asia.
"""

summary = """
Many of ICON’s industrial customers remain quite conservative about the introduction of cloud-based modelling into both their own organisations and also into their supplier base, over whose decisions they often exert significant influence.  In extreme cases, exploration of cloud solutions by some manufacturers is allegedly banned internally.  A shortage of convincing success stories also limits the speed of change in the status quo.  The EU Fortissimo (www.fortissimo-project.eu) project produced results which clearly demonstrated the market for cloud based simulation and in one case highlighted the potential benefits for a European SME automotive supercar manufacturer, namely Koenigsegg.  The market and concept were proven and lessons were also learnt which indicated the need for an improved end user experience through a simple intuitive web-enabled user interface designed for the cloud.  Following the conclusions from Fortissimo, ICON subsequently developed a prototype CFD solution designed specifically for the cloud, which was piloted with a Tier-1 automotive supplier and a wind turbine design consultancy.  The prototype is a web based platform allowing access to ICON’s simulation technologies.   The feedback from the pilot studies was generally positive but also highlighted some necessary future developments in order to achieve a full productive solution for Cloud based CFD for industry.  These recommendations include: improved decision making and evaluation tools, security certification, replication, independent security audit, legal contract preparation; and finally optimisation of the management of large data sets produced by CFD.  These issues remain to be resolved to demonstrate flexibility to deploy the CFD solution for widespread productive industrial use.
"""

role = """
Many of ICON’s industrial customers remain quite conservative about the introduction of cloud-based modelling into both their own organisations and also into their supplier base, over whose decisions they often exert significant influence.  In extreme cases, exploration of cloud solutions by some manufacturers is allegedly banned internally.  A shortage of convincing success stories also limits the speed of change in the status quo.  The EU Fortissimo (www.fortissimo-project.eu) project produced results which clearly demonstrated the market for cloud based simulation and in one case highlighted the potential benefits for a European SME automotive supercar manufacturer, namely Koenigsegg.  The market and concept were proven and lessons were also learnt which indicated the need for an improved end user experience through a simple intuitive web-enabled user interface designed for the cloud.  Following the conclusions from Fortissimo, ICON subsequently developed a prototype CFD solution designed specifically for the cloud, which was piloted with a Tier-1 automotive supplier and a wind turbine design consultancy.  The prototype is a web based platform allowing access to ICON’s simulation technologies.   The feedback from the pilot studies was generally positive but also highlighted some necessary future developments in order to achieve a full productive solution for Cloud based CFD for industry.  These recommendations include: improved decision making and evaluation tools, security certification, replication, independent security audit, legal contract preparation; and finally optimisation of the management of large data sets produced by CFD.  These issues remain to be resolved to demonstrate flexibility to deploy the CFD solution for widespread productive industrial use.

Based on the industrial experience outlined above, ICON will provide CFD software and related cloud service experience to the proposed project with the following responsibilities: Definition of use cases in collaboration with end users, analysis and specification of end user requirements, development of pilot web apps and support to service providers and end-users of new toolkits for decision making and results evaluation, exploitation and dissemination of results to an international customer base through a commercially available platform. ICON has already been investing in the development of a web based platform allowing access to ICON’s normally on premise simulation technologies.  “iconPlatform” as it is called is designed to be a unique approach to simulation through the deployment of use-case specific “Apps”.  These Apps are focused on ease of use, and robustness.   The goal was to provide all the tools necessary to rapidly develop, deploy, and maintain simulation Apps for engineers. The Apps, have a simple look and feel, and are based on a straightforward workflow where the user can access CFD technology.

iconPlatform will be used in the project to run the CFD applications (“apps”) using remote computing facilities provided by other partners in the project.
"""


[[projects]]
name = "Fortissimo"
description = """
EU Fortissimo (www.fortissimo-project.eu) - The EU Fortissimo project produced results which clearly demonstrated the market for cloud based simulation and in one case highlighted the potential benefits for a European SME automotive supercar manufacturer, namely Koenigsegg. ICON was a major participant in the successful Experiment 417 in particular.
"""

[[personnel]]
name = "Simon Weston"
gender = "male"
description = """
is Managing Director of ICON.  Simon received his MSc diploma in Advanced Mechanical Engineering from Imperial College (London) in 1996 and has spent the last 19 years years consulting to industry for the company.
"""

[[personnel]]
name = "Jacques Papper"
gender = "male"
description = """
is Technical Director of ICON.  Jacques received his MSc from the aeronautical school in ENSICA, France in 2006.  Before joining ICON in 2008 Jacques was employed by Aircraft Research Association as a Technology Leader. Jacques has led the development of innovative web based CFD technologies within ICON over the past 3 years bringing previously only on-premise technology to the cloud.
"""

+++

