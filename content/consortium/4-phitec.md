+++
weight = 4
type = "consortium"
name = "Phitec Ingegneria S.r.l."
abbr = "PHITEC"
logo = "img/logos/phitec.png"
website = "http://www.phitecingegneria.it"

description = """
Phitec (Phitec Ingegneria Srl) was founded in 2002 as a consulting company specialized only in numerical simulations. The vision of the company was to provide state-of-the-art simulation services in all structural field, ranging from linear static analysis to vibration analysis ending with the most complex crash/safety simulations. To achieve this goal Phitec has invested a large amount of time in the creation of specific know-how that is accessible within the company intranet by a Wikipedia like software tool. Advanced skills has been grown in the field of scientific programming, complex visualization (such as volumetric rendering of Big Data) and automatic optimization using open-source software tools (as an example the usage of DAKOTA for CFD optimization of wind turbine blades). After several year of activities, focused on the structural analysis field a new department specialized in CFD analysis has been created, the experience of using open-source CFD software for solving industrial related problems goes back to 2007, a number of national and international conference paper has been presented.
"""

summary = """
Phitec (Phitec Ingegneria Srl) was founded in 2002 as a consulting company specialized only in numerical simulations. The vision of the company was to provide state-of-the-art simulation services in all structural field, ranging from linear static analysis to vibration analysis ending with the most complex crash/safety simulations. To achieve this goal Phitec has invested a large amount of time in the creation of specific know-how that is accessible within the company intranet by a Wikipedia like software tool. Advanced skills has been grown in the field of scientific programming, complex visualization (such as volumetric rendering of Big Data) and automatic optimization using open-source software tools (as an example the usage of DAKOTA for CFD optimization of wind turbine blades). After several year of activities, focused on the structural analysis field a new department specialized in CFD analysis has been created, the experience of using open-source CFD software for solving industrial related problems goes back to 2007, a number of national and international conference paper has been presented.
"""

role = """
Phitec has the interest of providing to his customers a state-of-the-art cloud based simulation solution in order to let them focus on the engineering of new products rather that dealing with the technical aspects of setting up simulations and extracting relevant data from them. Before giving full access to this tool Phitec is willing to test it in detail. Phitec is going to provide test cases and will setup a small working group with the task of testing the infrastructure from the point of view of:
* ease of use, evaluated as the number of information that an engineer should provide to the system in order to obtain a valid CFD calculation
* speed of execution of the simulation
* speed of extraction of relevant data from the simulation
* reliability in case of non optimal connection

Two different test cases will be provided:
* small test case, with this test case Phitec has the need of evaluating the setup speed of the cloud environment this speed is of particular interest because during stochastic optimization there is the need of evaluation of many small variants 
* small-medium test case, this case has the scope of evaluation the speed of execution of the simulation but also the speed (and quality) of extraction of simulation data such as pictures, graphs of relevant quantities and other synthetic indexes needed to assess the engineering content of the test case

Phitec is a consultancy company that provide services for different customers, within this scenario his engineers are able to collect the data needed, organize the simulation submission and judge simulation results. A team of skilled engineers will provide workforce and technical skills to support the deployment of the tasks

Phitec will support ICON iconPlatform in the optimal application (“apps”) web frontend setup by bringing his clients needs reflected into the web page.
"""

[[personnel]]
name = "Daniele Speziani"
gender = "male"
description = """
is the Technical Director of Phitec Ingegneria. Technical skills: 20 years of experience in the field of numerical simulations, CAE project management in the automotive filed, experienced in HPC system design with focus on cluster achitectures, experienced in technical software development for CAE environment. 11 papers presented at national and international conferences/seminaries. Currently leading Phitec Ingegneria R&D group with the aim of expanding/transferring simulation services and know-how from mature (such as automotive) to new fast-growing fields.
"""

[[personnel]]
name = "Fabio Vicenza"
gender = "male"
description = """
is Phitec CFD lead engineer. He degreed on Aerospace Engineering at Turin technical university, after working for Piaggio Aero Industries and PECOFacet Italy as project engineer he joined Phitec. Since 2013 he has developed Phitec skills on open source CFD software by working with major Italian Automotive OEM: Lamborghini, FIAT, CNH, Ducati, Piaggio.
"""

+++

