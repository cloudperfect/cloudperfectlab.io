+++
weight = 8
type = "consortium"
name = "Cognity S.A."
abbr = "COG"
logo = "img/logos/cog.png"
website = "http://www.cognity.gr/"

description = """
Cognity is one of the leading, comprehensive System Integrators in the region of Southeast Europe. The company’s mission is to add value to its Customers and help them achieve their business goals, by promoting, implementing and supporting globally leading IT solutions. Cognity has successfully implemented large-scale, mission-critical projects, applying globally acclaimed solutions of Portals & Content Management (including eCare and eCommerce), CRM, End-to-End Order Management, Billing, SOA-based Integration, Mobile Applications and website mobilization, BPM, IT Service Management, Master Data Management, Data Quality, Marketing Automation, Business Intelligence and Analytics.

Cognity, an ISO 9001:2008 certified company, offers comprehensive Solutions and Services focusing primarily on the top organizations of the market, through a team of highly qualified professionals with extensive experience in Southeast Europe. The complete product and service offering of Cognity includes high-calibre consulting services and turnkey projects targeting mainly in Telecommunications, Banking, Insurance, Consumer Goods and Pharmaceuticals. 

Cognity’s deep understanding of the business domains, combined with its extensive experience in Business Processes allows the company to be an active partner and consultant to customer organizations, enabling the integration of new systems to their business and technical landscape.
"""

summary = """
Cognity is one of the leading, comprehensive System Integrators in the region of Southeast Europe. The company’s mission is to add value to its Customers and help them achieve their business goals, by promoting, implementing and supporting globally leading IT solutions. Cognity has successfully implemented large-scale, mission-critical projects, applying globally acclaimed solutions of Portals & Content Management (including eCare and eCommerce), CRM, End-to-End Order Management, Billing, SOA-based Integration, Mobile Applications and website mobilization, BPM, IT Service Management, Master Data Management, Data Quality, Marketing Automation, Business Intelligence and Analytics.
"""

role = """
Congity has extensive experience in the following domains: 
* Web Portals, eCommerce and eCare, Web Knowledge & Content Management Solutions:
* Customer Relationship Management (CRM) & Customer Experience Management
* End-to-End Order Management (OM)
* Enterprise Application Integration (EAI), enabled by Service Oriented Architectures (SOA), Enterprise Service Bus (ESB) and extended by Business Process Management (BPM) 
* IT Service Management & Enterprise Security
* Billing & Revenue Management
In the context of the project, Cognity will act as an application provider, bringing in the application to be used in the SaaS UC and exploring various business model approaches in the context of this UC, as well as pricing schemes for a Cloud based deployment.
"""


[[personnel]]
name = "Ioannis Stavroulas"
gender = "male"
description = """
is an experienced manager with more than 15 years of experience in enterprise solutions architecture and delivery management, having held various research, engineering and management positions in this time. In 2005 he took over Cognity’s Professional Services practice with the task to grow and cultivate the company’s software delivery organization, which today numbers more than 200 IT consultants and software engineers. He has been involved in the company’s major projects, and oversaw its R&D activities. 
In 2006 he obtained his PhD in Information Integration from the National Technical University of Athens. He also holds a Physics degree from the University of Athens, an MSc in Computer Science from the University of Essex and a joint MBA from the University of Piraeus, the University of Athens and the National Technical University of Athens. 
"""

[[personnel]]
name = " Dr. Dimitris Charilas Nikos Paraskevopoulos"
gender = "male"
description = """
"""



#[[personnel]]
#name = "Penelope Mantzari"
#gender = "female"
#description = """
#is an IT and Telecommunications professional with 20 years of experience in these fields. Since 2007 she joined Cognity’s Sales Consulting Department as Manager and has been responsible for the solution design and architecture in all its major projects since. She has become an expert in Telecommunications enterprise systems, designing solutions for international multi-million Euro projects (Europe, Balkans & Middle East), and leading multi-disciplined teams (developers, implementation engineers and project managers).  From 1990 to 2005 she worked as a Telco OSS & BSS Business Unit/Business Support Systems Manager, Billing Section Manager in the Solutions Division/Business Development and Solution Synthesis Department. She graduated (in 1984) in Chemical Engineering from the National Technical University of Athens and was awarded a PhD (in 1990) in the area of Process Modelling, Optimisation and Control at the same university.
#"""
#
#[[personnel]]
#name = "Harry Ladas"
#gender = "male"
#description = """
#has a BSc in Computer Science from the Technological Institute of Athens (in 1997), an MSc in Telematics from the University of Sheffield (in 1998) where he was also awarded his PhD in Mobile Communications, in 2000. From 2000 to 2005 he worked as a technical architect, an R&D Team Leader and a Software analyst & designer focusing on products delivering added value to mobile handsets. From 2006 to 2013 he worked as a Technical Product Manager and Software Architect in Self Service Solutions. In 2013 he joined Cognity’s Professional Services Department as a Senior Consultant and he has been actively involved in the analysis, design and implementation phases of several e-commerce projects focusing on self-service, e-shop and corporate site solutions for telco and retail companies.
#"""
#
#[[personnel]]
#name = "Charalambos Papadopoulos"
#gender = "male"
#description = """
#graduated (in 2014) in Informatics and Telecommunications from the University of Athens and is now pursuing a PhD in Advanced Information Systems. In 2014 he joined Cognity’s Professional Services Department as a junior Consultant and where he works in projects involving the development of complete e-shop solutions for tangible goods and services, addressing desktop and mobile devices.
#"""

+++

