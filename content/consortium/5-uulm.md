+++
weight = 5
type = "consortium"
name = "Ulm University"
abbr = "UULM"
logo = "img/logos/uulm.png"
website = "http://www.uni-ulm.de/"

description = """
Ulm University is a public university in the city of Ulm, in the South German state of Baden-Württemberg, one of the economically strongest regions of Europe with more than 11 million inhabitants. The university was founded in 1967 and focuses on natural sciences, medicine, engineering sciences, mathematics, economics and computer science. It is regarded one of the best universities in the world in the field of Optoelectronics, RF Circuit Design and Microelectronics. It ranks among the top universities in Germany for Electrical Engineering and Computer Science. Ulm University is represented by the Institute for Information Resource Management (OMI). The institute is closely connected to the central service unit communication and information centre (Kommunikations- und Informationszentrum - kiz) of Ulm University responsible for all IT services, media services, digital printing and the university library.

The institute is a member of the Faculty of Engineering, Computer Science and Psychology and is fully engaged in the teaching curriculum. It further has a strong research and development background in embedded and parallel systems, as well as in clouds and distributed systems.
"""

summary = """
Ulm University is a public university in the city of Ulm, in the South German state of Baden-Württemberg, one of the economically strongest regions of Europe with more than 11 million inhabitants. The university was founded in 1967 and focuses on natural sciences, medicine, engineering sciences, mathematics, economics and computer science. It is regarded one of the best universities in the world in the field of Optoelectronics, RF Circuit Design and Microelectronics. It ranks among the top universities in Germany for Electrical Engineering and Computer Science. Ulm University is represented by the Institute for Information Resource Management (OMI). The institute is closely connected to the central service unit communication and information centre (Kommunikations- und Informationszentrum - kiz) of Ulm University responsible for all IT services, media services, digital printing and the university library.
"""

role = """
OMI contributes with its experience, gained from the participation and coordination of multiple European projects over the last years. OMI members participating in CloudPerfect come from the cloud and distributed systems group and have several years of experiences developing software and middleware for cloud computing, loosely coupled systems, and network technology. With its expertise in cloud computing and distributed architecture UULM has developed the Cloudiator toolkit, an extensible framework for the deployment, adaption, monitoring, and management of applications in a heterogeneous cloud environment. It supports various private and public cloud providers (e.g. Amazon, Google and Openstack) and is able to deploy a modelled application across multiple clouds. It is actively used in the EU projects PaaSage, CACTOS and CloudSocket. The group has an established software development and integration process supported by tools including build and integration server and issue tracker.
"""

contributions = """
The institute operates a cluster system comprising >16 dual socket nodes with different configurations including many-core systems (included in the following tables). A top-of-the-rack switch connects the 20 server nodes. The switch offers Software Defined Networking (SDN) and is programmable using the OpenFlow protocol and e.g. OpenDaylight as SDN controller software installed on the OpenFlow control node. Regarding security aspects, no direct access to the compute nodes from the outside, public network is possible. The special nodes (control, network and storage node) are publicly accessible and must have well-configured firewalls and similar software installed to provide security. The public IP addresses for the virtual machines are translated by the network node, where firewall rules are applied to secure traffic to the virtual infrastructure.

Furthermore, the desktop computing infrastructure used for teaching purposes in the microcontroller lab (25 seats) is operated in dual-boot configuration and is available for computing experiments (including GPGPUs). Furthermore, the institute operates a couple of more experimental systems ranging from very low power compute systems such as Raspberry Pi, BeagleBone Black, FPGA based systems as well as Atom based mini-clusters, and a 10-node Parallela based cluster system.

Additionally, the institute has full access to the regional 500 node cluster system bwUniCluster as well as to the >500 node computing systems provided by kiz. 

"""

[[projects]]
name = "FP7-PaaSage"
description = """
Within FP7-PaaSage (Model-based Cloud Platform Upperware, EU FP7 Grant Agreement No. 317715, http://www.paasage.eu/) UULM is a member of the executive board and has in particular contributed to the Execution Management solutions.
"""
[[projects]]
name = "FP7-CACTOS"
description = """
As coordinator of the FP7-CACTOS project (Context-Aware Cloud Topology Optimisation and Simulation, EU FP7 Grant Agreement No. 610711, http://www.cactosfp7.eu/) the major technical contribution had been to realise an integration of placement and migration algorithms within OpenStack that go far beyond the off the shelf solutions enabling complex optimisation and placement algorithms in particular for resource demanding cloud applications.
"""
[[projects]]
name = "CloudSocket"
description = """
UULM is acting as scientific coordinator of the H2020 project CloudSocket (Grant Agreement 644690, https://www.cloudsocket.eu/) and is in particular responsible to realise solutions allowing to map Business Process as a Service Bundles on different Cloud Middleware Infrastructure from IaaS, PaaS up to SaaS solutions.
"""
[[projects]]
name = "bwCloud"
description = """
In the bwCloud project (funded by the state ministry MWK,  http://bwcloud.uni-mannheim.de/) UULM participates in the development of a Cloud computing infrastructure, distributed over the different state universities. The goal is to achieve maximum availability and performance by cooperatively operating the infrastructure, while keeping the costs for infrastructure and support low.
"""

[[personnel]]
name = "Prof. Dr.-Ing. Stefan Wesner"
gender = "male"
description = """
is the director of the Communication and Information Centre (KIZ) and the institute for Organisation and Management of Information systems (OMI) of the University of Ulm. Furthermore, he is the scientific chairman of the research network ISP connecting the 9 state universities and 40 applied research schools in the region. He has been actively participating in research projects on national and international level since 1997, focusing in particular on distributed systems and improving programmability and usability of large scale systems.
"""

[[personnel]]
name = "M. Sc. Daniel Baur"
gender = "male"
description = """
is a researcher and PhD student working in OMI’s research group on cloud computing. He is involved in the EC funded projects PaaSage and CACTOS. He is one of the lead developers of OMI’s cross-cloud orchestration platform Cloudiator. His current research interests include resource selection in heterogeneous systems, the description and the deployment and adaptation of applications on such systems.
"""

[[personnel]]
name = "Dr. Jörg Domaschka"
gender = "male"
description = """
is a senior researcher leading OMI’s research group on cloud computing, large-scale architectures, and adaptive middleware platforms. After graduation he worked several years as consultant for system integration before returning to academia. He has been involved in EC funded research project since 2006 and is currently UULM’s lead scientist in the PaaSage as well as CloudSocket projects and holds the role of a project manager in the CACTOS project. His current research interests include fault-tolerance in distributed environments, self-adaptability and scalability, as well as programming paradigms for such systems.
"""

[[personnel]]
name = "Dipl. Inf. Lutz Schubert"
gender = "male"
description = """
is head of the research group on heterogeneous and embedded systems at OMI and coordinates the research activities at the institute in this field. He has been the executive coordinator of the research projects S(o)OS (EC-FET) and ECOUSS (BMBF), and the designated rapporteur of the EC’s Cloud Expert Group. His current research focuses in particular on improved performance and scale in embedded devices through data localisation. He participates actively in both the HPC and cloud communities on topics of hybrid resource infrastructures and programmability.
"""

+++

