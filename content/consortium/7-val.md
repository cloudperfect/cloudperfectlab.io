+++
weight = 7
type = "consortium"
name = "VALEO THERMAL SYSTEMS"
abbr = "VAL"
logo = "img/logos/val.png"
website = "http:// www.valeo.com"

description = """
Valeo is an independent Group fully focused on the design, the manufacturing and the sale of components, integrated systems and modules for the automotive industry, ranking among the world’s top OEMs. Its R&D strategy is an important lever for its growth and it deposits 800 patents per year. Its Thermal Systems Business Group (Valeo Thermal Systems, VTS) mostly designs systems that manage the thermal energy of all types of powertrain aiming to significantly reduce fuel consumption, CO2 emissions and other pollutants and harmful particles from vehicles and to increase travel range and battery life for hybrid and electric vehicles. VTS has established important simulation resources which aim to the understanding of physics, to improve the turnover of studies and consequently to reduce costs, making the virtual development a reality as many products pass through the simulation offices from the first design drafts. Performance prediction, optimization techniques and even the reliability assessment are increasingly supported by HPC. With a long year expertise and through the team’s collaboration with numerous national and international academic institutions and enterprises, VTS has brought the simulation in the R&D department, through efficient and affordable techniques for industrial companies and nowadays the deployment of an ambitious simulation network depends only on the extent of available numerical resources.
"""

summary = """
Valeo is an independent Group fully focused on the design, the manufacturing and the sale of components, integrated systems and modules for the automotive industry, ranking among the world’s top OEMs. Its R&D strategy is an important lever for its growth and it deposits 800 patents per year. Its Thermal Systems Business Group (Valeo Thermal Systems, VTS) mostly designs systems that manage the thermal energy of all types of powertrain aiming to significantly reduce fuel consumption, CO2 emissions and other pollutants and harmful particles from vehicles and to increase travel range and battery life for hybrid and electric vehicles. VTS has established important simulation resources which aim to the understanding of physics, to improve the turnover of studies and consequently to reduce costs, making the virtual development a reality as many products pass through the simulation offices from the first design drafts. Performance prediction, optimization techniques and even the reliability assessment are increasingly supported by HPC. With a long year expertise and through the team’s collaboration with numerous national and international academic institutions and enterprises, VTS has brought the simulation in the R&D department, through efficient and affordable techniques for industrial companies and nowadays the deployment of an ambitious simulation network depends only on the extent of available numerical resources.
"""

role = """
Valeo will be mainly involved in the cloud simulation aspects, by investigating different questions and experiment with methods regarding data transfer (simulation set-up, mesh, results), submission of works, queuing time, and remote post-processing among others.

In this context, VTS will offer its case studies, which cover a wide range of simulation types, sizes of model and time calculations, using both commercial software and open-source code. In order to make a fair assessment of the strengths and weakness of cloud simulation, a wide range of technical cases will be tested. For each of them, Valeo will provide necessary data to ensure the proper progressing of the simulation process by providing clear description of the simulation case, geometries and/or meshing, existing numerical and/or experimental results for measuring the correctness of the calculations. In addition, a post-doctoral researcher already qualified and skilled in complex numerical models and High Power Computing (HPC) will be financed in order to develop relevant processes and methodologies. These models will be tested during the project, and a fair comparison of simulation time between internal cluster and cloud solution will be done and documented.
"""


[[projects]]
name = "Pepito"
description = """
(supported by the French Agency for Research) is developing innovative techniques for DOE, surrogate models and turbomachine optimization based on HPC
"""
[[projects]]
name = "Cascade"
description = """
(supported by the French national fund for numerical simulation) is experimenting new numerical methodologies for the development of Air Intake Module. The consortium includes several partners from the aeronautic domain, the main one being Safran Turbomeca.
"""
[[projects]]
name = "Climb"
description = """
(supported by the French national fund for numerical simulation) is aimed to bring the new Latice Boltzmann solver Labs on the market. Main partners are Airbus and Renault.
"""
[[projects]]
name = "Ecoquest"
description = """
supported by the FP7 European program, 2009-2012) was aimed to develop innovative techniques for the design, the optimization and the integration of cooling module used in ground transportation
"""
[[projects]]
name = "Expamtion"
description = """
(supported by the pole of Competence Systematic) has experimented a collaborative design methodology involving all players of the supply chain using simulation on a shared cluster.
"""

[[personnel]]
name = "Dr. Manuel Henner"
gender = "male"
description = """
earned his engineering degree from the Ecole Nationale d’Ingénieur de Belfort in 1991, and then obtained a master degree from the Institute of Fluid Mechanics of Strasbourg in 1993. He spend one year as a research assistant employed by the CNRS (Centre National de Recherches Scientifiques) at the University of Poitiers, where he continued by doing a thesis obtained in 1998 in the domain of Mechanics, Energetics and Thermics. He joined Valeo In 1998 where he has been contributing to the development and to the integration;n of the numerical process for several products. Thanks to his contributions in the field of research, with 21 patents and regular contributions in congresses or scientific publications, he was nominated as an Expert in 2003, and as a Senior Expert in 2012. He is now Head of the Simulation and Reliability Department for Valeo Thermal Systems, and he provides regular contribution to funded projects, such as Ecoquest from F7 program. He has about 24 years of experience in simulation, with 18 years in industrial R&D departments.
"""

[[personnel]]
name = "Dr. Stéphane Moreau"
gender = "male"
description = """
obtained his engineering degree and MSc from ISAE- Sup’Aéro (France) in 1988 and his PhD in Mechanical Engineering with a minor in Aeronautics and Astronautics from Stanford University in 1993. He then worked for a start-up company AC2 on plasma physics in 1994 where he helped develop the plasma micro-thruster concept used nowadays on most satellites. He then worked for a year at the turbo-engine builder Snecma on nozzle designs (Safran group). Late 1995 he joined the automotive Tier-1 supplier Valeo where he worked for 13 years on engine cooling fan system design. He joined the Mechanical Engineering faculty of Université de Sherbrooke in 2009 as an associate Professor. He became a full professor in 2011. His research topics include aeroacoustics, turbomachinery design and CFD (Computational Fluid Dynamics). He has about 350 scientific publications with more than two-third in aeroacoustics with significant contributions in analytical noise modeling, experimental noise measurements and large scale numerical aeroacoustic simulations (requiring high power computing for instance).
"""

+++

