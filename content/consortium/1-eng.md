+++
weight = 1
type = "consortium"
name = "Engineering Ingegneria Informatica S.p.A."
abbr = "ENG"
logo = "img/logos/eng.png"
website = "http://www.eng.it/"

description = """
Engineering was founded in 1980, and it is currently the first IT group in Italy, among the top 10 IT groups in Europe, with approx. 7.400 employees and 40 branch offices in Italy, Belgium, Latin America and USA. The group produces IT innovation to more than 1.000 large clients, with a complete offer combining system and business integration, outsourcing, cloud services, consulting, and proprietary solutions. Engineering Data Centres offer business continuity and IT infrastructure management to about 15.000 servers and 230.000 workstations. In 2014, consolidated revenues are 853 millions of euro, with a share of 7% of domestic market, and 12% of annual turnover resulting from overseas activities. Engineering operates through 7 different business units: Finance, Central Government, Local Government and Healthcare, Energy & Utilities, Industry and Telecoms, delivering innovative IT solutions to main vertical markets: Aerospace, Insurance, Automotive, Banks, Consumer Products, Defence and Aerospace, Energy & Utilities, Training, Central & Local Government, Homeland Security, Life Science, Manufacturing, Media, International Organisation, Retail, Healthcare, Telecommunications, Transports, Welfare.
Since 1987, Engineering innovation capability is supported by its Central Unit of Research & Development, with around 250 researchers currently involved in over 70 research projects co-funded by national and international authorities. The R&D Unit is located across 6 different locations in Italy and in Europe, with a shared investment of 25 millions of euro for the current year. Engineering holds different responsibilities within the international research community, including technical and overall co-ordination of large research projects and consortia. In particular, the company is core partner of EIT ICT Labs in Italy (European Institute of Innovation and Technology) focused on leveraging ICT for Quality of Life; founding partner of the Big Data Value Association and of the Future Internet PPP initiative; member of the Board of EOS (European Organisation for Security); core partner of NESSI (Networked European Software and Service Initiative). Engineering is an active member of most international open source communities and founder of SpagoWorld, a free/open source initiative managed by Engineering. The company is corporate member of OW2 Consortium and Eclipse Foundation.
"""

summary = """
Within the Engineering R&D Unit, the Distributed Computing Lab is committed to the investigation of new and innovative models and technologies for distributed systems. During the years, the team acquired a deep knowledge in - and experimented several - Cloud technologies to distribute and manage computing and storage. The team gained expertise in technical, business and sustainability aspects of Cloud technologies participating in several EU projects in the past years. 
"""

role = """
Within the Engineering R&D Unit, the Distributed Computing Lab is committed to the investigation of new and innovative models and technologies for distributed systems. During the years, the team acquired a deep knowledge in - and experimented several - Cloud technologies to distribute and manage computing and storage. The team gained expertise in technical, business and sustainability aspects of Cloud technologies participating in several EU projects in the past years. 

In addition to its expertise in Cloud, Engineering brings in CLOUDPERFECT a toolset for Cloud benchmarking (the Benchmarking Suite) developed in the ARTIST project and capable of automating the process of execution of tests on several supported Cloud infrastructures. Engineering will improve the Benchmarking Suite with new tests, more supported Cloud infrastructures and new result analysis features. 

Finally, Engineering make available in the project a research-focused Cloud infrastructure hosted in Engineering premises to support the realization and operation of pilot cases in the project, and will take part in the pre-market evaluation phase from the scope of a Cloud provider. 
"""

contributions = """
Research projects are supported by a section of the Engineering’s infrastructure fully isolated from the production one and managed to satisfy specific projects needs, both for development, testing and demo purposes. The research infrastructure is composed by 12 servers for a total of about 180 cores, 360 GB of RAM and 10 TB storage. The software stack operating operating the infrastructure is currently based on the RedHat’s oVirt datacenter management software. It allows the management of the hardware as well as virtual resources in the infrastructure, both via a web portal and REST API. In particular the facility is consisted of:

* Fujitsu-Siemens Primergy RX300 S4 with 2 Quad-Core Intel® Xeon® 64-bit and 8GB    RAM
* Fujitsu-Siemens Primergy RX200/RS300 S6 with 8 Quad-Core Intel® Xeon® 64-bit ,32GB RAM
* Dell PowerEdge 1950 servers with 2 Dual-Core Intel® Xeon® 5100 3.0GHz 64-bit and 8GB RAM
* Storage array Dell AX4-5i set-up to provide 10TB of disk space

The software stack operating operating the infrastructure is currently based on the RedHat’s oVirt[33] datacenter management software. It allows the management of the hardware as well as virtual resources in the infrastructure, both via a web portal and REST API.

Engineering is a founding member of the FIWARE community and part of the FIWARE Lab: a distributed Cloud infrastructure deployed over 15 geographic regions spread all over Europe and America Latina. In total, it offers a capacity of more than 3000 cores, 10 TB RAM, 500 TB disk and is going to be further expanded with the addition of new regions/nodes. Engineering will make available nodes of FIWARE Lab to the CLOUDPERFECT pilot cases.

"""

[[projects]]
name = "ARTIST (FP7-317859)"
description = "offers a set of methods and tools which provide an end-to-end and assisted migration service to transform non-cloud software applications not merely to run on cloud but to take full advantage of cloud features."

[[personnel]]
name = "Isabel Matranga"
gender = "female"
description = """
has a degree in Political Sciences from the University of Palermo. She has been working for ENG group since 2001. Since 2006 she has worked in the field of distributed computing at ENG R&D laboratory. She cooperates with the head of the ENG R&D unit on ‘Infrastructures for distributed computing’ mainly taking care of the dissemination of project results, the identification of exploitation and sustainability strategies and the management of EU funded projects. She was deputy of the project director in the VENUS-C project and ERINA+ support action. She supported dissemination and exploitation activities in both the above projects and took care of the identification of a sustainability plan for the TEFIS testbed. She is now involved in the OCEAN project, (http://www.ocean-project.eu) which aims at developing an EU-Japan joint roadmap for the interoperability and industry take up of open source cloud software. She is also leader of the dissemination and exploitation activities in VENIS project (EU co-funded project) and supports dissemination and exploitation activities in ClouT project (EU co-funded project on Cloud and IoT). Her main interests evolve around communication and exploitation strategies to raise awareness on research results and support their uptake both in the research and the commercial domain.
"""

[[personnel]]
name = "Andrea Manieri"
gender = "male"
description = """
is graduated in Computer Science 1998, with a master thesis on object oriented lambda calculus with prof. Emeritus C. Boehm. In 2000 he joined EU projects as Technical Manager in the ECOLNET, then in 2002 he was appointed as RTD Coordinator in Engisanità Spa, an Engineering Group company on HealthCare market. On April 2003 he joined again the Engineering labs as responsible of development of new business on Grid Technology establishing a specific unit and contributing to several project in FP6/FP7 project until 2013. In this period he was Exploitation Manager of Diligent project, and Project manager of and EC study (www.erina-study.eu). He was also Project Director of VENUS-C project (FP7 INFRA-261556) and ERINA+ support Action (FP7 INFRA-261550). His Distributed Computing Laboratory contributed to more than twenty FP6 and FP7 initiatives ranging from AAA in distributed environments, Test and quality assurance of distributed software, grid and cloud infrastructures, infrastructure security and legal compliance. He’s trainer in the company Academy “Enrico della Valle” for the Cloud technologies. In H2020 EC funding program he is coordinating the Company initiatives in the LEIT (ICT and SPACE) and Excellent Science Pillars. Currently is responsible of the Data Science Research team and is coordinating the design the Cloud Computing courses at the ENG Academy.
"""

[[personnel]]
name = "Gabriele Giammatteo "
gender = "male"
description = """
raduated (in 2009) in Computer Engineering at the Università degli studi di Roma Tre on Secure Storage in distributed environments. He is employed in Engineering's R&D Lab as software engineer working in the setup and management of distributed infrastructures in particular in grid and cloud computational and data management systems. He has been involved in EU FP7 and H2020 projects since 2009 focused on various topics of cloud computing software engineering ranging from software integration, testing and quality assurance (D4Science, iMarine, OCEAN, S-CASE, BlueBRIDGE), to benchmarking (ARTIST), to automation (TEFIS, EGI-ENGAGE), to cloud ecosystem sustainability (OCEAN).
"""

[[personnel]]
name = "Nunzio Andrea Galante"
gender = "male"
description = """
graduated (in 2005) in Computer Science at the Università degli studi di Perugia on Network Systems and (in 2014) in Computer Engineering at the Università degli Studi Guglielmo Marconi di Roma on Software Engineering. From 2006 to 2013 he was involved in project management, solution design & delivery projects on large-scale systems in Telco companies (mainly Telecom Italia). In 2013 he joined Engineering's R&D Lab as a Software Engineer working in design of solutions for the management of distributed infrastructures with a focus on cloud computing. He was involved in FP7 ARTIST project and currently in two different H2020 projects: EGI-Engage and BlueBRIDGE.
"""

+++

