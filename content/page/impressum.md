+++
title="Impressum"
+++

## Publisher

Ulm University  
D-89069 Ulm  
Phone +49 (0)731 / 50-10  
Fax +49 (0)731 / 50-22038  
sales tax identification number DE173703203

## Representation

Ulm University is a public body and is represented by its President, Prof. Dr.-Ing. Michael Weber, or its Chief Financial Officer, Dieter Kaufmann.

## Address of the President

Ulm University
President Prof. Dr.-Ing. Michael Weber
Helmholtzstrasse 16
89081 Ulm
Germany

## Address of the Chief Financial Officer

Ulm University
Chief Financial Officer Dieter Kaufmann
Helmholtzstrasse 16
89081 Ulm
Germany

## Bank details

Kasse der Universität Ulm
Bundesbank Ulm
BIC (SWIFT Code): MARKDEF1630
IBAN: DE72 6300 0000 0063 0015 05 

## Content

The responsibility for the contents of websites of the CloudPerfect website lies with the Institute of Resource Management led by Prof. Dr.-Ing. Stefan Wesner.

## Responsible regulating authority

Ministry of Science, Research and the Arts Baden-Württemberg
Königstraße 46
70173 Stuttgart
Germany 

## Copyright

© Universität Ulm and the CloudPerfect consortium

All contents published on this website (layouts, texts, pictures, graphics etc.) are subject to copyright. Any use that is not authorised according to copyright law requires the previous explicit written agreement from Ulm University. This particularly applies to copying, editing, translating, saving, processing and playing back contents in databases or other electronic media and systems. Photocopies and downloads of websites for private, academic and non-commercial use are permitted.

Ulm University explicitly allows and welcomes citations of documents and websites as well as linking to its website.

Ulm University strives to honour copyrights of graphics, audio documents, video sequences and texts and to use either its own or license-free graphics, audio documents, video sequences and texts in all its publications.

All brand names and trademarks mentioned anywhere on this web site may be protected by third parties are subject to the regulations of the respective trademark law and the right of ownership of their registered proprietor. Mentioning alone is no reason to conclude that trademarks are not protected by third-party rights!

## Disclaimer

### Exclusion of liability for own contents

The CloudPerfect consortium makes every effort to ensure that the information and data contained on its website is correct. However, we can accept no liability or guarantee that the information and data provided is up-to-date, correct and complete. We particularly assume no liability for potential damages or consequences resulting from direct or indirect use of the provided contents. Please send any requests for amendments of contents to the project coordinator

## Exclusion of liability for links to external contents

According to general German law, we are responsible for contents that we provides on the internet. It is important to distinguish between  own contents and references or hyperlinks to the contents of other providers ('third-party contents'). These third-party contents are neither created by the CloudPerfect consortium, nor does the consortium have any influence on contents of third-party websites. 
The contents of third-party websites to which this website references via links do not reflect the opinion of the CloudPerfect consortium and serves merely to provide additional information and context. We do not take ownership of or assume liability for these linked third-party contents.
This applies to all links and references within this internet presence. Liability for illegal, erroneous and incomplete contents and, above all, for damages resulting from the use or disregard of information presented in this manner, shall lie solely with the provider of the page to which any of these links refer and not with the CloudPerfect consortium. 

 