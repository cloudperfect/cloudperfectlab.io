+++
title = "Get Started with CloudPerfect"
draft = false
menu = "main"
+++
**Thank you for your interest in CloudPerfect!**

Work on CloudPerfect has started on December 2016, the first tools and documentation will be available soon (see below). Check back soon or [get in touch](mailto:isabel.matranga@eng.it) to learn more.

# Publication Roadmap

## The CloudPerfect Architecture

The architecture specifies the tools and services CloudPerfect provides to the users and providers. 
The interface specifications, communication patterns and information flow is described in this document. 

The architecture will be released in two iterations, so please check back then:

* version 1: May 2017
* version 2: October 2018


## The CloudPerfect Toolkit

CloudPerfect will provide tools and services for the following capabilities:

* Benchmarking suites
* Performance and SLA monitoring
* Performance prediction
* Cloud orchestration
* Visualisation tools
* and many more

The tools will be made available from October 2017 on


## Experiments and Demonstrators

The capabilities of CloudPerfect will be assessed through a set of "experiments" that will be available for viewing and testing. We currently foresee the following scenarios:

* Computational Fluid Dynamics
* ERP / CRM

A full description of the scenarios will be made available in May 2017.

The first experiments will be available from October 2017 on.


# Background Software

The CloudPerfect toolkits built up on a set of background software already available right now. Please follow the links below for more information on these:

* 3ALib
* ARTIST Benchmarking Suites
 * [YCSB](https://github.com/brianfrankcooper/YCSB/wiki) is a tool for benchmarking common database solutions for the cloud.
 * Dwarfs is an implementation of the Berkeley's 13 dwarfs
 * [Cloudsuite](http://cloudsuite.ch/) is a suite of eight benchmarks based on very popular, real-world cloud applications
(e.g. Data Caching, Data Serving, Graph Analytics, Media Streaming) using real stacks and setups;
 * [FileBench](http://filebench.sourceforge.net/) is a very flexible file system and storage benchmarking tool. 
 * [DaCapo](http://dacapobench.org/) is a collection of benchmarks specifically designed to facilitate performance analysis of Java Virtual Machines, compilers and memory management.
* SLALOM Open Reference Model
* [Cloudiator](http://cloudiator.org/): a cloud orchestration and deployment toolset.
* IRMOS Overhead Prediction Model
* ARTIST Classification Tools
